<?php

namespace Osd\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=255)
     */
    private $userName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_created", type="datetime")
     */
    private $userCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_expired", type="datetime")
     */
    private $userExpired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_activated", type="boolean")
     */
    private $isActivated;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return User
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    
        return $this;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set userCreated
     *
     * @param \DateTime $userCreated
     * @return User
     */
    public function setUserCreated($userCreated)
    {
        $this->userCreated = $userCreated;
    
        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \DateTime 
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userExpired
     *
     * @param \DateTime $userExpired
     * @return User
     */
    public function setUserExpired($userExpired)
    {
        $this->userExpired = $userExpired;
    
        return $this;
    }

    /**
     * Get userExpired
     *
     * @return \DateTime 
     */
    public function getUserExpired()
    {
        return $this->userExpired;
    }

    /**
     * Set isActivated
     *
     * @param boolean $isActivated
     * @return User
     */
    public function setIsActivated($isActivated)
    {
        $this->isActivated = $isActivated;
    
        return $this;
    }

    /**
     * Get isActivated
     *
     * @return boolean 
     */
    public function getIsActivated()
    {
        return $this->isActivated;
    }
}