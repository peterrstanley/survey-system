<?php

namespace Osd\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Osd\UserBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * @Route("/login")
     * @Template()
     */
    public function indexAction(Request $request)
    {
		
		if($request->get('sign_in') !== null){
			$user = $request->get('username');
			$pass = hash('sha256',$request->get('password'));
			$em = $this->GetDoctrine()->getManager();
			$repository = $em->getRepository('OsdUserBundle:User');
			
			$user = $repository->FindOneBy(array('email' => $user, 'password' => $pass));
			if(!isset($user)){
				return $this->render('OsdUserBundle:Default:login.html.twig', array('error' => true, 'error_msg' => "Username/Password invalid."));
			}
        	return $this->render('OsdUserBundle:Default:login.html.twig', array('error' => true, 'error_msg' => "Username Found"));
		} elseif($request->get('sign_up') !== null){
			return $this->redirect($this->generateUrl('osd_user_default_signup'));
		}
		
		return $this->render('OsdUserBundle:Default:login.html.twig', array('error' => false, 'error_msg' => ""));
	}
	
	/**
     * @Route("/signup")
     * @Template()
     */
     public function signupAction(Request $request){
     	if($request->get('register') !== null){
     		$em = $this->GetDoctrine()->getManager();
			
			$email = $request->get('email');
			$pass = hash('sha256',$request->get('password'));
			$pass2 = hash('sha256',$request->get('password2'));
			
			$user = $repository->FindOneBy(array('email' => $email));
			if(isset($user)){
				return $this->render('OsdUserBundle:Default:signup.html.twig', array('error' => true, 'error_msg' => "Account already created with provided email."));
			}
			if($pass !== $pass2){
				return $this->render('OsdUserBundle:Default:signup.html.twig', array('error' => true, 'error_msg' => "Passwords do not match."));
			}
			$created = new \DateTime();
			$expired = new \DateTime();
			$expired->setDate(2100,12,31)->setTime(23,59,59);
			$user = new User();
			$user->setUsername($email);
			$user->setEmail($email);
			$user->setPassword($pass);
			$user->setUserCreated($created);
			$user->setUserExpired($expired);
			$user->setIsActivated(true);
			$em->persist($user);
			$em->flush();
			
			return $this->redirect($this->generateUrl('osd_user_default_index'));
		}
     	return $this->render('OsdUserBundle:Default:signup.html.twig', array('error' => false, 'error_msg' => ""));
     }
}
