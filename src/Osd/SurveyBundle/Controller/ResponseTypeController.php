<?php

namespace Osd\SurveyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Osd\SurveyBundle\Entity\ResponseType;
use Osd\SurveyBundle\Form\ResponseTypeType;

/**
 * ResponseType controller.
 *
 * @Route("/developer/responsetypes")
 */
class ResponseTypeController extends Controller
{

    /**
     * Lists all ResponseType entities.
     *
     * @Route("/", name="developer_responsetypes")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('OsdSurveyBundle:ResponseType')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new ResponseType entity.
     *
     * @Route("/", name="developer_responsetypes_create")
     * @Method("POST")
     * @Template("OsdSurveyBundle:ResponseType:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new ResponseType();
        $form = $this->createForm(new ResponseTypeType(), $entity);
        $form->submit($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('developer_responsetypes_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new ResponseType entity.
     *
     * @Route("/new", name="developer_responsetypes_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new ResponseType();
        $form   = $this->createForm(new ResponseTypeType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a ResponseType entity.
     *
     * @Route("/{id}", name="developer_responsetypes_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('OsdSurveyBundle:ResponseType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ResponseType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ResponseType entity.
     *
     * @Route("/{id}/edit", name="developer_responsetypes_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('OsdSurveyBundle:ResponseType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ResponseType entity.');
        }

        $editForm = $this->createForm(new ResponseTypeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing ResponseType entity.
     *
     * @Route("/{id}", name="developer_responsetypes_update")
     * @Method("PUT")
     * @Template("OsdSurveyBundle:ResponseType:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('OsdSurveyBundle:ResponseType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ResponseType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ResponseTypeType(), $entity);
        $editForm->submit($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('developer_responsetypes_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a ResponseType entity.
     *
     * @Route("/{id}", name="developer_responsetypes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->submit($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('OsdSurveyBundle:ResponseType')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ResponseType entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('developer_responsetypes'));
    }

    /**
     * Creates a form to delete a ResponseType entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
