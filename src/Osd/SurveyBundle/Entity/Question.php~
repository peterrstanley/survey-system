<?php

namespace Osd\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity
 */
class Question
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="text")
     */
    private $question;

    /**
     * @var integer
     *
     * @ORM\Column(name="response_type", type="integer")
     */
    private $responseType;

    /**
     * @var string
     *
     * @ORM\Column(name="options", type="text")
     */
    private $options;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_required", type="boolean", nullable=true)
     */
    private $isRequired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_enabled", type="boolean", nullable=true)
     */
    private $isEnabled;

	/**
	 * @ORM\OneToOne(targetEntity="ResponseType")
	 * @ORM\JoinColumn(name="response_type",referencedColumnName="id")
	 */
	protected $types;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set responseType
     *
     * @param integer $responseType
     * @return Question
     */
    public function setResponseType($responseType)
    {
        $this->responseType = $responseType;
    
        return $this;
    }

    /**
     * Get responseType
     *
     * @return integer 
     */
    public function getResponseType()
    {
        return $this->responseType;
    }

    /**
     * Set options
     *
     * @param string $options
     * @return Question
     */
    public function setOptions($options)
    {
        $this->options = $options;
    
        return $this;
    }

    /**
     * Get options
     *
     * @return string 
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set isRequired
     *
     * @param boolean $isRequired
     * @return Question
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;
    
        return $this;
    }

    /**
     * Get isRequired
     *
     * @return boolean 
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     * @return Question
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;
    
        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean 
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set types
     *
     * @param \Osd\SurveyBundle\Entity\ResponseType $types
     * @return Question
     */
    public function setTypes(\Osd\SurveyBundle\Entity\ResponseType $types = null)
    {
        $this->types = $types;
    
        return $this;
    }

    /**
     * Get types
     *
     * @return \Osd\SurveyBundle\Entity\ResponseType 
     */
    public function getTypes()
    {
        return $this->types;
    }
}